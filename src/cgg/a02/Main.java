package cgg.a02;

import java.util.Arrays;

import cgtools.Image;

public class Main {

    public static void main(String[] args) {

        //to-do:    - Anzahl Abtastung richtig einstellen
        
        //auf neuem Image die Sample Methode aufrufen und ColoredDiscs Sampler uebergeben
        ColoredDiscs data = new ColoredDiscs();
        Arrays.sort(data.coloredDiscs);

        //Point Sampling
        Image image = new Image();
        image.sample(data, false);

        //Stratified Sampling
        Image image2 = new Image();
        image2.sample(data, true);

        // Write the images to disk.
        final String filename1 = "doc/a02-discs.png";
        image.write(filename1);
        System.out.println("Wrote image: " + filename1);

        final String filename2 = "doc/a02-discs-supersampling.png";
        image2.write(filename2);
        System.out.println("Wrote image: " + filename2);
    }
}
