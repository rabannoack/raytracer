package cgg.a02;

import cgtools.Color;
import cgtools.Sampler;
import static cgg.MyValues.*;

public class ColoredDiscs implements Sampler {
 
    public int number, width, height, maxRadius;
    public final Disc[] coloredDiscs;
    public final Color backgroundColor;

    public ColoredDiscs(int number, int width, int height, int maxRadius, Color backgroundColor) {
        this.number = number;
        this.width = width;
        this.height = height;
        this.maxRadius = maxRadius;
        this.backgroundColor = backgroundColor;
        this.coloredDiscs = createAllDiscs();
    }

    public ColoredDiscs(int number){
        this();
        this.number = number;
    }

    //default constructor
    public ColoredDiscs(){
        this(ANZAHL_DISCS, WIDTH, HEIGHT, MAX_RADIUS, BACKGROUND_COLOR);
    }

    private Disc[] createAllDiscs(){
        Disc[] discArray = new Disc[this.number];
        for (int i = 0; i < this.number; i++){
            discArray[i] = Disc.randomDisc(this.width, this.height);
        }
        return discArray;
    }

    @Override
    public String toString() {
        String s = "";
        for(Disc d : this.coloredDiscs) s += d.toString();
        return s;
    }

    @Override
    public Color getColor(double x, double y) {
        for(Disc d : this.coloredDiscs){
            if(d.isPointInDisc(x, y)) return d.color;     
        }
        return this.backgroundColor; 
    } 
}
