package cgg.a02;

import cgtools.Color;
import cgtools.Random;
import static cgg.MyValues.*;


public class Disc implements Comparable<Disc> {
    
    public final int x, y, radius;
    public final Color color;

    public Disc(int x, int y, int radius, Color color) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
    }

    public Disc(int[] coordinates, int radius, Color color){
        this(coordinates[0], coordinates[1], radius, color);
    }

    public static Disc randomDisc(int width, int height) {
        return new Disc(randomCoordinates(width, height), 
                    new Random().nextInt(MAX_RADIUS), 
                    Color.getRandomColor());
    }

    public boolean isPointInDisc(double x, double y){
        double dis = Math.sqrt(Math.pow((x - this.x), 2) + Math.pow((y - this.y), 2));
        return  dis <= this.radius;
    }

    private static int[] randomCoordinates(int width, int height){
        int x = new Random().nextInt(width);
        int y = new Random().nextInt(height);
        return new int[]{x, y};
    }

    public static int getRadius(Disc d){
        return d.radius;
    }

    @Override
    public String toString() {
        return "[X:" + this.x 
                + " - Y:" + this.y 
                + " - Radius:" + this.radius 
                + " - Color:" + this.color 
                + "]\n";
    }

    @Override
    public int compareTo(Disc s) {
        return Integer.compare(this.radius, s.radius);
    }
}
