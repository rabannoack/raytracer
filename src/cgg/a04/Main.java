package cgg.a04;

import cgtools.Cam;
import cgtools.Color;
import cgtools.Group;
import cgtools.Image;
import cgtools.RayTracer;
import cgtools.Shape;
import cgtools.shapes.Background;
import cgtools.shapes.Plane;
import cgtools.shapes.Sphere;

import static cgtools.Vector.*;
import static cgtools.shapes.Sphere.*;

public class Main {
    
    public static void main(String[] args) {

        int width = 480;
        int height = 270;

        Cam c = new Cam(Math.PI / 3, point(0, 0, 0), width, height);

        Shape background = new Background(color(0.1, 0.5, 0.5));
        Shape ground = new Plane(point(0, -0.5, -150), 500.0, Color.gray, direction(0, 1, 0.15)) ;
        Shape globe1 = new Sphere(25, point(-35, 0, -125), Color.red);
        Shape globe2 = new Sphere(20, point(0, 0, -125), Color.green);
        Shape globe3 = new Sphere(25, point(35, 0, -125), Color.blue);
        
        Group group = new Group(new Shape[]{ground, background, globe1, globe2, globe3});
        RayTracer r1 = new RayTracer(c, group);

        Image image1 = new Image();
        image1.sample(r1, true);
        
        final String filename1 = "doc/a04-3-spheres.png";
        image1.write(filename1);
        System.out.println("Wrote image: " + filename1);

//---------------------------------------------------------------------------------

        Cam c2 = new Cam(Math.PI / 4, point(0, 0, 0), width, height);

        Shape background2 = new Background(Color.black); 
        Shape ground2 = new Plane(point(0, -40, -150), 500.0, color(0.1, 0.1, 0.2), direction(0.2, 0.8, 0.15));

        //FIXME: wenn direction.z == 0 dann Nullpointer-Exception
        Shape ground4 = new Plane(point(0, 0, -150), 30.0, color(1, 1, 0), direction(0.5, 0.5, 0.2));
        Shape g1 = new Sphere(23, point(0, 0, -150), color(0.1, 0.1, 0.4));
        Shape g2 = getRandomSphere(10, 480, 270, -150);
        Shape g3 = getRandomSphere(10, 480, 270, -150);
        Shape g4 = getRandomSphere(10, 480, 270, -150);
        Shape g5 = getRandomSphere(10, 480, 270, -150);
        Shape g6 = getRandomSphere(10, 480, 270, -150);
        Shape g7 = getRandomSphere(10, 480, 270, -150);
        Shape g8 = getRandomSphere(10, 480, 270, -150);
        Shape g9 = getRandomSphere(10, 480, 270, -150);
        Shape g10 = getRandomSphere(10, 480, 270, -150);

        Group group2 = new Group(new Shape[]{ground2, ground4, background2, 
                                    g1, g2, g3, g4, g5, g6, g7, g8, g9, g10});

        RayTracer r2 = new RayTracer(c2, group2);
        
        Image image2 = new Image();
        image2.sample(r2, true);
        
        final String filename2 = "doc/a04-scene.png";
        image2.write(filename2);
        System.out.println("Wrote image: " + filename2);
    }
}
