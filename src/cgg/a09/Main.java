package cgg.a09;

import cgtools.RayTracer;
import cgtools.Cam;
import cgtools.Group;
import cgtools.Image;
import cgtools.Matrix;
import cgtools.Shape;
//import cgtools.Transformation;
import cgtools.shapes.Background;
import cgtools.shapes.Cylinder;
//import cgtools.shapes.Pipe;
import cgtools.shapes.Plane;
//import cgtools.shapes.Ring;
import cgtools.shapes.Sphere;

import static cgtools.Vector.*;

import java.util.List;

import static cgtools.Matrix.*;
import static cgtools.Material.*;

public class Main {
    
    public static void main(String[] args) {

        final long start = System.currentTimeMillis();

        // 4K
        //int width = 3840; int height = 2160;
        
        // Full-HD
        int width = 1920; int height = 1080;
    
        // Test-Resolution    
        //int width = 480; int height = 270;

        Group group = new Group(
            new Plane(point(0, 0, 0), 50.0, direction(0, 1, 0), TEXTURE_02),
            new Group(Sphere.makeSpheres(point(0, 0.9, 0), 2)), //------>
            new Sphere(2, point(2, 4, -5), PERFECT_MIRROR),
            //new Sphere(5, point(-10, 4, -3.5), PERFECT_MIRROR),
            new Cylinder(3, 4, point(-10, 0, -3.5), TEXTURE_05),
            new Background(TEXTURE_BACKGROUND_04)
        );

        List<Shape> shapeList = Group.getAllShapes(group);

        Group test = new Group(new Shape[shapeList.size()]);
        for(int i = 0; i < shapeList.size(); i++){
            test.shapes[i] = shapeList.get(i);
        }
        test = Group.bvh(test);
        

        Matrix view = identity();
        view = multiply(translation(0, 0, 20), view);
        view = multiply(rotation(direction(1, 0, 0), -15), view);
        view = multiply(rotation(direction(0, 1, 0), 30), view);
        Cam cam2 = new Cam(1, point(0, 0, 0), width, height, view);

        RayTracer r1 = new RayTracer(cam2, test); //------>

        Image image1 = new Image(width, height, new double[width * height * 3], 100);
        image1.sample(r1, true);
        
        final String filename1 = "doc/a08-2-thread-test_05.png";
        image1.write(filename1);
        System.out.println("Wrote image: " + filename1); 
        
        final long end = System.currentTimeMillis();
        final double timeSec = (end - start) / 1000;
        System.out.println("Laufzeit: " + timeSec + "sec = " + timeSec / 60 + "min");
    }
}
