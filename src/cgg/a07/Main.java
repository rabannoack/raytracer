/** @author rabannoack@web.de */
package cgg.a07;

import cgtools.RayTracer;
import cgtools.Cam;
import cgtools.Group;
import cgtools.Image;
import cgtools.Matrix;
import cgtools.Shape;
import cgtools.shapes.Background;
import cgtools.shapes.Cylinder;
import cgtools.shapes.Pipe;
import cgtools.shapes.Plane;
import cgtools.shapes.Ring;
import cgtools.shapes.Sphere;

import static cgtools.Vector.*;
import static cgtools.Matrix.*;
import static cgtools.Material.*;

public class Main {

    public static void main(String[] args) {

        // 4K
        //int width = 3840; int height = 2160;
        
        // Full-HD
        //int width = 1920; int height = 1080;
    
        // Test-Resolution    
        int width = 480; int height = 270;
        
        Shape lamp1 = new Plane(point(0, 150, -150), 210.0, direction(0, 1, 0), GLOWING_MATERIAL_WHITE);
        Shape lamp2 = new Ring(point(0, 149, -150), 190.0, 100.0, direction(0, 1, 0), GLOWING_MATERIAL_BLUE);
        Shape lamp3 = new Ring(point(0, 0, 55), 55.0, 15.0, direction(0, 0, 1), GLOWING_MATERIAL_VIOLET);

        Shape b = new Background(BACKGROUND_MATERIAL_BLACK);

        Shape pl1 = new Plane(point(0, -10, -300), 500.0, direction(0, 1, 0.01), LAMBERT_MATERIAL_ROSA);
        Shape pl2 = new Plane(point(0, -9.99, -300), 500.0, direction(0, 1, 0.01), GLASS);

        Shape s1 = new Sphere(10, point(0, -7, -80), GLASS_BROWN);
        Shape s6 = new Sphere(5, point(15, -8, -50), GLASS_LIGHT_PURPLE);
        Shape s7 = new Sphere(1.6, point(-8.5, -8.0, -50), GLASS_LIGHT_GREEN);
        Shape s8 = new Sphere(3, point(-25, -9.5, -65), GLASS_LIGHT_BLUE);
        Shape c1 = new Cylinder(20, 40, point(-30, -15, -150), PERFECT_MIRROR);
        Shape c2 = new Cylinder(10, 35, point(50, -10, -100), MATT_MIRROR_BLUE);
        Shape c9 = new Pipe(12, 5, 35, point(65, -40, -60), MATT_MIRROR_2);
        //Shape c10 = new Pipe(35, 5, 15, point(-100, -15, -170), LAMBERT_MATERIAL_RED);
        Shape c11 = new Pipe(35.1, 5.2, 15.1, point(-100, -15.1, -170), GLASS);
        Shape c12 = new Pipe(10, 3, 30, point(-100, -15, -170), MATT_MIRROR_BLUE);
        Shape c3 = new Cylinder(8, 15, point(-50, -10, -90), LAMBERT_MATERIAL_ORANGE);
        Shape c4 = new Cylinder(8.1, 15.2, point(-50, -10, -90), GLASS);
        Shape c5 = new Cylinder(8, 6, point(-5, -15, -50), MATT_MIRROR_1);
        //Shape c5 = new cgtools.Pipe(10, 4, 8, point(-5, -15, -50), MATT_MIRROR_1);

        Shape c7 = new Cylinder(2, 2.5, point(-2, -10, -50), LAMBERT_MATERIAL_RED);
        Shape c8 = new Cylinder(2.01, 2.51, point(-2, -10, -50), GLASS);
        Shape c6 = new Cylinder(20, 20, point(-200, 20, 10), LAMBERT_MATERIAL_RED);
        Shape s2 = new Sphere(25, point(0, 40, -200),MATT_MIRROR_2);
        Shape s3 = new Sphere(25, point(40, 25, -160),LAMBERT_MATERIAL_ORANGE);
        Shape s4 = new Sphere(25.1, point(40, 25, -160),GLASS);
        Shape s5 = new Sphere(30, point(-30, -20, 50),LAMBERT_MATERIAL_BLUE);
        
        
        Matrix view = identity();
        view = multiply(translation(0, 55, 110), view);
        view = multiply(rotation(direction(1, 0, 0), -25), view);
        
        //Cam c = new Cam(1, point(0, 0, 0), width, height);
        Cam cam2 = new Cam(1, point(0, 0, 0), width, height, view);

        Group group = new Group(new Shape[]{lamp1, lamp2, lamp3,
                                            b, 
                                            pl1, pl2, 
                                            c1, c2, c3, c4, c5, c6, c7, c8, c9, /*c10,*/ c11, c12,
                                            s1, s2, s3, s4, s5, s6, s7, s8});

        RayTracer r1 = new RayTracer(cam2, group);

        Image image1 = new Image(width, height, new double[width * height * 3], 100);
        image1.sample(r1, true);
        
        final String filename1 = "doc/test_new_class_arragement.png";
        image1.write(filename1);
        System.out.println("Wrote image: " + filename1);
    }

}
