package cgg.a08;

import cgtools.RayTracer;
import cgtools.Cam;
import cgtools.Group;
import cgtools.Image;
import cgtools.Matrix;
import cgtools.Material;
import cgtools.Shape;
import cgtools.Transformation;
import cgtools.shapes.Background;
import cgtools.shapes.Cylinder;
import cgtools.shapes.Pipe;
import cgtools.shapes.Plane;
import cgtools.shapes.Ring;
import cgtools.shapes.Sphere;

import static cgtools.Vector.*;
import static cgtools.Matrix.*;
import static cgtools.Material.*;

public class Main {
    
    public static void main(String[] args) {

        final long start = System.currentTimeMillis();

        //@ FIXME entferne "b" aus groups + neu strukturieren
        //@ FIXME für Raueme von innen lieber HollowCylinder statt Pipe

        // 4K
        //int width = 3840; int height = 2160;
        
        // Full-HD
        //int width = 1920; int height = 1080;
    
        // Test-Resolution    
        int width = 480; int height = 270;

        Shape b = new Background(BACKGROUND_MATERIAL_WHITE);

        Matrix move2 = identity();
        move2 = multiply(rotation(direction(0, 0, 1), -90), move2);
        move2 = multiply(translation(0, 50, 0), move2);
        move2 = multiply(rotation(direction(0, 1, 0), -45), move2);
        Transformation t2 = new Transformation(move2);

        Group cylinders2 = new Group(t2, new Shape[]{
            new Cylinder(10, 100, point(-40, 0, 0), randomMaterial()),
            new Cylinder(10, 80, point(-20, 0, 0), randomMaterial()),
            new Cylinder(10, 60, point(0, 0, 0), randomMaterial()),
            new Cylinder(10, 40, point(20, 0, 0), randomMaterial()),
            new Cylinder(10, 20, point(40, 0, 0), randomMaterial()),
            new Group(new Shape[]{}),
        });

        Matrix move3 = identity();
        move3 = multiply(rotation(direction(0, 0, 1), 90), move3);
        move3 = multiply(translation(0, 50, 0), move3);
        move3 = multiply(rotation(direction(0, 1, 0), 45), move3);
        Transformation t3 = new Transformation(move3);
        Group cylinders3 = new Group(t3, new Shape[]{
            new Cylinder(10, 20, point(-40, 0, 0), randomMaterial()),
            new Cylinder(10, 40, point(-20, 0, 0), randomMaterial()),
            new Cylinder(10, 60, point(0, 0, 0), randomMaterial()),
            new Cylinder(10, 80, point(20, 0, 0), randomMaterial()),
            new Cylinder(10, 100, point(40, 0, 0), randomMaterial()),
        });

        Matrix move4 = identity();
        move4 = multiply(rotation(direction(0, 0, 1), -90), move4);
        move4 = multiply(translation(0, 50, 0), move4);
        move4 = multiply(rotation(direction(0, 1, 0), 45), move4);
        Transformation t4 = new Transformation(move4);
        Group cylinders4 = new Group(t4, cylinders2.shapes);

        Matrix move5 = identity();
        move5 = multiply(rotation(direction(0, 0, 1), 90), move5);
        move5 = multiply(translation(0, 50, 0), move5);
        move5 = multiply(rotation(direction(0, 1, 0), -45), move5);
        Transformation t5 = new Transformation(move5);
        Group cylinders5 = new Group(t5, cylinders3.shapes);

        Matrix move6 = identity();
        move6 = multiply(translation(30, 107, -30), move6);
        Transformation t6 = new Transformation(move6);
        Group ashtray = new Group(t6, new Shape[]{
            new Cylinder(20, 3, point(0, 0, 0), randomMaterial()),
            new Pipe(20, 3, 5, point(0, 3, 0), randomMaterial()),
        });

        Matrix move7 = identity();
        move7 = multiply(rotation(direction(0, 0, 1), 180), move7);
        move7 = multiply(translation(0, 175, 0), move7);
        Transformation t7 = new Transformation(move7);
        Group desklamp = new Group(t7, new Shape[]{
            new Cylinder(40, 3, point(0, 0, 0), randomMaterial()),
            new Plane(point(0, 3.3, 0), 30.0, direction(0, 1, 0), GLOWING_MATERIAL_WHITE),
            new Pipe(40, 3, 8, point(0, 3, 0), randomMaterial()),
            //new Cylinder(0.6, 100, point(0, -100, 0), LAMBERT_MATERIAL_GREY),
        });

        Matrix move8 = identity();
        move8 = multiply(rotation(direction(0, 0, 1), 90), move8);
        move8 = multiply(translation(55, 114.5, -30), move8);
        Transformation t8 = new Transformation(move8);
        Group cigarette = new Group(t8, new Shape[]{
            new Cylinder(1.5, 6, point(0, 0, 0), LAMBERT_MATERIAL_ORANGE),
            new Cylinder(1.5, 8, point(0, 6, 0), LAMBERT_MATERIAL_GREY),
            new Cylinder(1.3, 2, point(0, 14, 0), GLOWING_MATERIAL_RED),
        });

        Group seats = new Group(new Shape[]{
            new Cylinder(50, 50, point(120, 0, -80), randomMaterial()),
            new Cylinder(50, 50, point(-120, 0, -80), randomMaterial()),
            new Cylinder(50, 50, point(120, 0, 80), randomMaterial()),
            new Cylinder(50, 50, point(-120, 0, 80), randomMaterial()),
            new Cylinder(49, 3, point(120, 50, -80), randomMaterial()),
            new Cylinder(49, 3, point(-120, 50, -80), randomMaterial()),
            new Cylinder(49, 3, point(120, 50, 80), randomMaterial()),
            new Cylinder(49, 3, point(-120, 50, 80), randomMaterial()),
        });

        Matrix move9 = identity();
        move9 = multiply(translation(50, 0, -100), move9);
        Transformation t9 = new Transformation(move9);
        Group seat = new Group(t9, new Shape[]{
            b,
            new Cylinder(42, 8, point(0, 40, 0), LAMBERT_MATERIAL_RED),
            new Cylinder(8, 40, point(-20, 0, -20), MATT_MIRROR_1),
            new Cylinder(8, 40, point(20, 0, -20), MATT_MIRROR_1),
            new Cylinder(8, 40, point(-20, 0, 0), MATT_MIRROR_1),
            new Cylinder(8, 40, point(20, 0, 0), MATT_MIRROR_1),
        });

        Group group = new Group(new Shape[]{
            new Group(new Shape[]{
                new Plane(point(0, 250, 0), 210.0, direction(0, 1, 0), GLOWING_MATERIAL_WHITE),
                //new Ring(point(0, 499, 0), 190.0, 100.0, direction(0, 1, 0), GLOWING_MATERIAL_BLUE),
                new Plane(point(0, 0, 0), 800.0, direction(0, 1, 0.01), LAMBERT_MATERIAL_ROSA),
                new Pipe(800, 20, 5, point(0, 10, 0), GLOWING_MATERIAL_WHITE),
                new Pipe(800, 5, 400, point(0, 15, 0), randomMaterial()),
                new Cylinder(19, 100, point(0, 0, 0), randomMaterial()),
                new Pipe(110, 20, 5, point(0, 100, 0), randomMaterial()),
                new Cylinder(110, 2, point(0, 105, 0), randomMaterial()),
                new Sphere(10, point(-40, 108, 20), randomMaterial()),
                new Cylinder(70, 3, point(0, 0, 0), randomMaterial()),
                new Sphere(100, point(0, 300, 0), GLOWING_MATERIAL_WHITE)
            }),
            //cylinders2,
            //cylinders3,
            //cylinders4,
            //cylinders5,
            ashtray,
            desklamp,
            cigarette,
            seats,
            b,
            //seat,
        });

        Matrix view = identity();
        view = multiply(translation(0, 60, 0), view);
        view = multiply(translation(0, 0, 400), view);
        view = multiply(rotation(direction(1, 0, 0), -12), view);
        //view = multiply(rotation(direction(0, 1, 0), 25), view);
        Cam cam2 = new Cam(1, point(0, 0, 0), width, height, view);

        RayTracer r1 = new RayTracer(cam2, group);

        Image image1 = new Image(width, height, new double[width * height * 3], 100);
        image1.sample(r1, true);
        
        final String filename1 = "doc/a08-2-thread-test_03.png";
        image1.write(filename1);
        System.out.println("Wrote image: " + filename1); 
        
        final long end = System.currentTimeMillis();

        System.out.println("Laufzeit: " + (end - start) + "millisec");
    }
}
