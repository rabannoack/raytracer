package cgg.a03;

import cgtools.Cam;
import cgtools.Image;
import cgtools.shapes.Sphere;

import static cgtools.Vector.*;

public class Main {
    
    public static void main(String[] args) {

        Cam c = new Cam((Math.PI / 4),point(0, 0, 0), 480, 270);

        /*
        Sphere s1 = new Sphere(20, point(-30, 0, -150), Color.red);
        Sphere s2 = new Sphere(20, point(0, 0, -150), Color.green);
        Sphere s3 = new Sphere(20, point(30, 0, -150), Color.blue);
        Sphere s4 = new Sphere(30, point(0, 0, -150), Color.white);
        RayTracer r = new RayTracer(c, new Sphere[]{s1, s2, s3, s4});
        */

        Sphere[] spheres = Sphere.getRandomSpheres(50, 20, c.width, c.height, -150, true);
        RayTracer r2 = new RayTracer(c, spheres);

        Image image2 = new Image();
        image2.sample(r2, true);
        
        final String filename2 = "doc/a03-three-spheres.png";
        image2.write(filename2);
        System.out.println("Wrote image: " + filename2);
    } 
}
