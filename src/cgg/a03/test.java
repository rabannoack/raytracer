package cgg.a03;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static cgtools.Vector.*;
import org.junit.Test;
import cgtools.*;
import cgtools.shapes.Sphere;


public class test {
    
    @Test
    public void test1(){
        Sphere s1 = new Sphere(1, point(0, 0, -2), Color.green);
        Ray r1 = new Ray(point(0, 0, 0), direction(0, 0, -1));
        assertEquals(point(0, 0, -1), s1.intersect(r1).hitPoint);
        assertEquals(direction(0, 0, 1), s1.intersect(r1).normalenVector);
    }

    @Test
    public void test2(){
        Sphere s1 = new Sphere(1, point(0, 0, -2), Color.green);
        Ray r2 = new Ray(point(0, 0, 0), direction(0, 1, -1));   
        assertNull(s1.intersect(r2));
    }

    @Test
    public void test3(){
        Sphere s3 = new Sphere(1, point(0, -1, -2), Color.green);
        Ray r3 = new Ray(point(0, 0, 0), direction(0, 0, -1));
        assertEquals(point(0, 0, -2), s3.intersect(r3).hitPoint);
        assertEquals(direction(0, 1, 0), s3.intersect(r3).normalenVector);
    }

    @Test
    public void test4(){
        Sphere s4 = new Sphere(1, point(0, 0, -2), Color.green);
        Ray r4 = new Ray(point(0, 0, -4), direction(0, 0, -1));
        assertNull(s4.intersect(r4));
    }

    @Test
    public void test5(){
        Sphere s5 = new Sphere(1, point(0, 0, -4), Color.green);
        Ray r5 = new Ray(point(0, 0, 0), direction(0, 0, -1), 0, 2);
        assertNull(s5.intersect(r5));
    }
}
