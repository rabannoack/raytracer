package cgg.a03;

import cgtools.Cam;
import cgtools.Color;
import cgtools.Hit;
import cgtools.Ray;
import cgtools.Sampler;
import cgtools.Util;
import cgtools.shapes.Sphere;

public class RayTracer implements Sampler { 
    
    public final Cam c;
    public final Sphere[] spheres;

    public RayTracer(Cam c, Sphere[] spheres){
        this.c = c;
        this.spheres = spheres;
    }

    @Override
    public Color getColor(double x, double y) {
        Ray r;
        for(Sphere s : this.spheres){
            r = this.c.getRay(x, y);
            Hit h = s.intersect(r);
            if(h == null){
                continue;
            } else {
                return Util.shade(h.normalenVector, h.material.albedo()); 
            }
        }
        return Color.black;  
    }
}
