package cgg.a06;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static cgtools.Vector.*;
import static cgtools.Material.*;
import static cgtools.Reflection.*;
import org.junit.Test;
import cgtools.*;
import cgtools.shapes.Sphere;

public class test {

    @Test
    public void test1_Sphere_intersect(){
        Sphere s1 = new Sphere(1, point(0, 0, -2), LAMBERT_MATERIAL_BLUE);
        Ray r1 = new Ray(point(0, 0, 0), direction(0, 0, -1), 0, Double.POSITIVE_INFINITY);
        assertEquals(point(0, 0, -1), s1.intersect(r1).hitPoint);
        assertEquals(direction(0, 0, 1), s1.intersect(r1).normalenVector);
    }

    @Test
    public void test2_Sphere_intersect(){
        Sphere s1 = new Sphere(1, point(0, 0, -2), LAMBERT_MATERIAL_BLUE);
        Ray r1 = new Ray(point(0, 0, -2), direction(0, 0, -1), 0, Double.POSITIVE_INFINITY);
        assertEquals(point(0, 0, -3), s1.intersect(r1).hitPoint);
        assertEquals(direction(0, 0, -1), s1.intersect(r1).normalenVector);
    }

    @Test
    public void test3_Sphere_intersect(){
        Sphere s1 = new Sphere(1, point(0, 0, -2), LAMBERT_MATERIAL_BLUE);
        Ray r1 = new Ray(point(0, 0, -4), direction(0, 0, -1), 0, Double.POSITIVE_INFINITY);
        assertNull(s1.intersect(r1));
    }

    @Test
    public void test_schlick(){
        assertEquals(13.9579, schlick(direction(0.707, 0.707, 0.000), direction(0, 1, 0), 1, 1.5), 0.01);
        assertEquals(13.9579, schlick(direction(0.707, 0.707, 0.000), direction(0, 1, 0), 1.5, 1), 0.01);
        assertEquals(0.608435, schlick(direction(0.995, -0.100, 0.000), direction(0, 1, 0), 1, 1.5), 0.01);
        assertEquals(0.608435, schlick(direction(0.995, -0.100, 0.000), direction(0, 1, 0), 1.5, 1), 0.01);
    }

    @Test
    public void test_refract(){
        Ray r = new Ray(point(0, 0, 0), (direction(0.707, 0.707, 0.000)));
        Hit h = new Hit(r, point(0, 0, 0), direction(0, 1, 0), LAMBERT_MATERIAL_BLUE);
        assertEquals(direction(0.471, -0.882, 0.000), refract(direction(0.707, 0.707, 0.000), direction(0, 1, 0), 1, 1.5, h, r));

    }  
}
