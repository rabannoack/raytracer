package cgg.a06;

import cgtools.RayTracer;
import cgtools.Cam;
import cgtools.Group;
import cgtools.Image;
import cgtools.Shape;
import cgtools.shapes.Background;
import cgtools.shapes.Plane;
import cgtools.shapes.Sphere;

import static cgtools.Vector.*;
import static cgtools.Material.*;

public class Main {
    
    public static void main(String[] args) {
        
        int width = 3840;
        int height = 2160;

        //int width = 1920;
        //int height = 1020;

        //int width = 1024;
        //int height = 768;

        //int width = 480;
        //int height = 270;
        
//---Scene1-------------------------------------------------------------------------------------------------------------------
       
        Shape lamp1 = new Plane(point(0, 90, -200), 85.0, direction(0, 1, 0), GLOWING_MATERIAL_WHITE);
        
        Shape b = new Background(BACKGROUND_MATERIAL_BLACK);

        Shape pl1 = new Plane(point(0, 0, -300), 500.0, direction(0, 1, 0.15), LAMBERT_MATERIAL_ROSA);
        Shape pl2 = new Plane(point(-60, 0, -180), 17.0, direction(0, 1, 0.15), GLOWING_MATERIAL_BLUE);
        Shape pl3 = new Plane(point(60, 0, -180), 17.0, direction(0, 1, 0.15), GLOWING_MATERIAL_GREEN);

        Shape s1 = new Sphere(200, point(0, 0, -400), GLASS);
        Shape s2 = new Sphere(15, point(0, 0, -200), LAMBERT_MATERIAL_BLUE);
        Shape s3 = new Sphere(15, point(-60, 0, -180), LAMBERT_MATERIAL_ORANGE);
        Shape s4 = new Sphere(15, point(60, 0, -180), LAMBERT_MATERIAL_RED);
        Shape s5 = new Sphere(15, point(-120, 0, -150), LAMBERT_MATERIAL_RED);
        Shape s6 = new Sphere(15, point(120, 0, -150), LAMBERT_MATERIAL_ORANGE);
        Shape s7 = new Sphere(15, point(-50, 50, -350), MATT_MIRROR_1);
        Shape s8 = new Sphere(15, point(50, 50, -350), MATT_MIRROR_2);
        Shape s9 = new Sphere(30, point(0, -25, -150), PERFECT_MIRROR);
        Shape s10 = new Sphere(15, point(0, 0, 20), LAMBERT_MATERIAL_BLUE);
        Shape s11 = new Sphere(15, point(-55, -20, -110), GLASS);
        Shape s12 = new Sphere(15, point(55, -20, -110), GLASS);
        
        Cam c = new Cam(1, point(0, 0, 0), width, height);

        Group group = new Group(new Shape[]{lamp1, b, pl1, pl2, pl3, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12});

        RayTracer r1 = new RayTracer(c, group);
        
//---Scene2-------------------------------------------------------------------------------------------------------------------       
        
        Shape bg = new Background(BACKGROUND_MATERIAL_BLACK);

        Shape gr = new Plane(point(0, 0, -300), 500.0, direction(0, 1, 0.15), LAMBERT_MATERIAL_ROSA);
        Shape p1 = new Plane(point(0, -14.21, -200), 50.0, direction(0, 1, 0.15), MATT_MIRROR_2);
        Shape p2 = new Plane(point(0, -14.22, -200), 55.0, direction(0, 1, 0.15), GLOWING_MATERIAL_BLUE);
        Shape p3 = new Plane(point(0, -14.23, -200), 60.0, direction(0, 1, 0.15), MATT_MIRROR_2);
        Shape p4 = new Plane(point(0, -14.24, -200), 65.0, direction(0, 1, 0.15), GLOWING_MATERIAL_WHITE);
        Shape p5 = new Plane(point(0, -14.25, -200), 70.0, direction(0, 1, 0.15), MATT_MIRROR_2);
        Shape p6 = new Plane(point(0, -14.26, -200), 75.0, direction(0, 1, 0.15), GLOWING_MATERIAL_VIOLET);
        Shape p7 = new Plane(point(0, -14.27, -200), 80.0, direction(0, 1, 0.15), MATT_MIRROR_2);
        Shape p8 = new Plane(point(0, -14.28, -200), 85.0, direction(0, 1, 0.15), GLOWING_MATERIAL_GREEN);
        Shape p9 = new Plane(point(0, -14.29, -200), 90.0, direction(0, 1, 0.15), MATT_MIRROR_2);

        Shape g1 = new Sphere(20, point(-73, 0, -200), LAMBERT_MATERIAL_ORANGE);
        Shape h1 = new Sphere(20.1, point(-73, 0, -200), GLASS);

        Shape g2 = new Sphere(20, point(-43, 15, -300), PERFECT_MIRROR);

        Shape g3 = new Sphere(20, point(43, 15, -300), LAMBERT_MATERIAL_BLUE);
        Shape h2 = new Sphere(20.1, point(43, 15, -300), GLASS);

        Shape g4 = new Sphere(20, point(73, 0, -200), LAMBERT_MATERIAL_RED);
        Shape h3 = new Sphere(20.1, point(73, 0, -200), GLASS);

        Shape g5 = new Sphere(25, point(0, 15, -200), GLASS);
        
        Shape l1 = new Sphere(30, point(0, 95, -200), GLOWING_MATERIAL_WHITE);
        Shape l2 = new Plane(point(0, 90, -200), 85.0, direction(0, 1, 0.15), GLOWING_MATERIAL_WHITE);

        Cam c2 = new Cam(1, point(0, 0, 0), width, height);

        Group group2 = new Group(new Shape[]{bg, gr, p1, p2, p3, p4, p5, p6, p7, p8, p9, g1, g2, g3, g4, g5, l1, l2, h1, h2, h3});

        RayTracer r2 = new RayTracer(c2, group2);

//----------------------------------------------------------------------------------------------------------------------
        
        //image constructor anpassen
        Image image1 = new Image(width, height, new double[width * height * 3], 100);
        image1.sample(r1, true);
        
        final String filename1 = "doc/a06-mirrors-glass-2.png";
        image1.write(filename1);
        System.out.println("Wrote image: " + filename1);
    }

}
