package cgg.a01;

import cgtools.*;

public class PolkaDots {

    public static int[] getSides(int width, int height, int columns, int rows){
        return new int[]{width / columns, height / rows};
    }

    public static int[] getAllCoordinates(int width, int height, int columns, int rows){
        //array speichert in Form {uX1, uY1, xC1, yC1, uX2,.....}
        int[] smallRectangles = getSides(width, height, columns, rows);
        int[] returnArray = new int[columns * rows * 4];       
        int xTemp = 0;
        int yTemp = 0;
        int breiteSmallRect = smallRectangles[0];
        int hoeheSmallRect = smallRectangles[1];

        for (int i = 0; i < returnArray.length; i += 4){
            int[] centerTemp = getCenter(xTemp, yTemp, breiteSmallRect, hoeheSmallRect);
            returnArray[i] = xTemp;
            returnArray[i + 1] = yTemp;
            returnArray[i + 2] = centerTemp[0];
            returnArray[i + 3] = centerTemp[1];
            xTemp += breiteSmallRect;
            if(xTemp >= width){
              xTemp = 0;
              yTemp += hoeheSmallRect;
            }
        }
        return returnArray;
    }

    public static int[] getCenter(int xUrsprung, int yUrsprung, int widthTemp, int heightTemp){
        int[] returnArray = {(xUrsprung + (widthTemp / 2)), (yUrsprung + (heightTemp / 2))};
        return returnArray;
    }

    public static Color getColor(int x, int y, int radius, int[] center, Color color) {
        if ((Math.sqrt(Math.pow((x - center[0]), 2) + Math.pow((y - center[1]), 2))) <= radius){
            return color;
        } else {
            return Color.black;
        }
    }
}

/*    
//................................................................................................
    //----> hier habe ich versucht aus Anzahl der Punkte automatisch eine
    //      Seitenaufteilung zu berechnen
    public static int[] getFactors(int dots){
        if(dots == 1) return new int[]{1, 1};
        //if(width == height) return int[]       
        int a = 1;
        double b = dots;        
        while ((b / 2) % 2 == 0){ //(b/2) klappt nicht immer manchmal muss man eins weiter
            a *= 2;
            b /= 2;
            if(b <= a) break;
        }
        return new int[]{a , (int)b};
    }

    public static int getMaxFactor(int[] factors){
        return Math.max(factors[0], factors[1]);
    }

    public static int getMinFactor(int[] factors){
        return Math.min(factors[0], factors[1]);
    }

    public static int[] getSides(int width, int height, int dots){
        int[] returnArray = new int[2];
        int[] factors = getFactors(dots);
        //laengere Seite durch groeszeren Faktor teilen
        if(width > height){
            returnArray[0] = width / getMaxFactor(factors);
            returnArray[1] = height / getMinFactor(factors);
        } else if (width < height){
            returnArray[0] = width / getMinFactor(factors);
            returnArray[1] = height / getMaxFactor(factors);
        } else {
            returnArray[0] = width / factors[0];
            returnArray[1] = width / factors[1];
        }
        return returnArray;
    }

    public static int[] getAllCoordinates(int width, int height, int dots){
        //array speichert in Form {uX1, uY1, xC1, yC1, uX2,.....}
        int[] smallRectangles = getSides(width, height, dots);
        int[] returnArray = new int[dots * 4];       
        int xTemp = 0;
        int yTemp = 0;
        int breiteSmallRect = smallRectangles[0];
        int hoeheSmallRect = smallRectangles[1];

        for (int i = 0; i < returnArray.length; i += 4){
            int[] centerTemp = getCenter(xTemp, yTemp, breiteSmallRect, hoeheSmallRect);
            returnArray[i] = xTemp;
            returnArray[i + 1] = yTemp;
            returnArray[i + 2] = centerTemp[0];
            returnArray[i + 3] = centerTemp[1];
            xTemp += breiteSmallRect;
            if(xTemp >= width){
              xTemp = 0;
              yTemp += hoeheSmallRect;
            }
        }
        return returnArray;
    }

*/

