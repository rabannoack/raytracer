package cgg.a01;

import cgtools.*;

public class Image {
  
  public final int width, height;
  public String filename;
  public final double[] data;
  //default werte für bit (im Moment 1) und 3 (rgb) um hardcoding zu vermeiden
  //Image(int, int) zu overloaded contructor machen
  //kompletten constructor einfügen

  public Image(int width, int height) {
    this.width = width;
    this.height = height;
    this.data = new double[width * height * 3];
  }

  public void setPixel(int x, int y, Color color) {
    int index = (y * width + x) * 3;
    this.data[index] = color.r;
    this.data[index + 1] = color.g;
    this.data[index + 2] = color.b;
  }

  public void write(String filename) {
    ImageWriter.write(filename, this.data, this.width, this.height);
  }

  public void sample(Sampler s) {
    notYetImplemented();
  }

  private void notYetImplemented() {
    System.err.println("Please complete the implementation of class cgg.Image as part of assignment 1.");
    System.exit(1);
  }
}
