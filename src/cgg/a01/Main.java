package cgg.a01;

import static cgtools.Color.*;
import cgg.*;

public class Main {

  public static void main(String[] args) {
    final int width = 480;
    final int height = 270;
    int radius = 14;
    int columns = 8;
    int rows = 5;
    //int dots = 24;

    // This class instance defines the contents of the image.
    ConstantColor content = new ConstantColor(gray);

    // Creates an image and iterates over all pixel positions inside the image.
    Image image = new Image();
    for (int x = 0; x != width; x++) {
      for (int y = 0; y != height; y++) {
        // Sets the color for one particular pixel.
        image.setPixel(x, y, content.getColor(x, y));
      }
    } 

    // Creates an image and iterates over all pixel positions inside the image.
    Image image2 = new Image();
    double[] center = Circle.getCenter(width, height);
    for (int x = 0; x != width; x++) {
      for (int y = 0; y != height; y++) {
        // Sets the color for one particular pixel.
        image2.setPixel(x, y, Circle.getColor((double)x, (double)y, radius, center));
      }
    } 
    
    // Creates an image and iterates over all pixel positions inside the image.
    int[] coordinates = PolkaDots.getAllCoordinates(width, height, columns, rows);
    int [] smallRectangles = PolkaDots.getSides(width, height, columns, rows);
    int breiteSmallRect = smallRectangles[0];
    int hoeheSmallRect = smallRectangles[1];
    Image image3 = new Image();  
    for(int a = 0; a < coordinates.length; a += 4){
      cgtools.Color color = getRandomColor();
      int xUrsprungTemp = coordinates[a];
      int yUrsprungTemp = coordinates[a + 1];
      int[] tempCenter = {coordinates[a + 2], coordinates[a + 3]};
      for (int x = xUrsprungTemp; x < (xUrsprungTemp + breiteSmallRect); x++) {
        for (int y = yUrsprungTemp ; y < (yUrsprungTemp + hoeheSmallRect); y++) {          
          // Sets the color for one particular pixel.
          image3.setPixelSmall(x, y, breiteSmallRect,PolkaDots.getColor(x, y, radius, tempCenter, color), columns);
        }
      }
    }
      
    // Write the image to disk.
    final String filename1 = "doc/a01-image.png";
    image.write(filename1);
    System.out.println("Wrote image: " + filename1);

    final String filename2 = "doc/a01-disc.png";
    image2.write(filename2);
    System.out.println("Wrote image: " + filename2);

    final String filename3 = "doc/a01-polka-dots.png";
    image3.write(filename3);
    System.out.println("Wrote image: " + filename3);
  }
}
