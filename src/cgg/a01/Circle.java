package cgg.a01;

import cgtools.*;

public class Circle {
    
    public static Color getColor(double x, double y, double radius, double[] center) {
        if ((Math.sqrt(Math.pow((x - center[0]), 2) + Math.pow((y - center[1]), 2))) <= radius){
            return Color.red;
        } else {
            return Color.black;
        }
    }

    public static double[] getCenter(double width, double height){
        double[] returnArray = {(width / 2), (height / 2)};
        return returnArray;
    }


}


