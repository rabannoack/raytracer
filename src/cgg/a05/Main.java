package cgg.a05;

import cgtools.RayTracer;
import cgtools.Cam;
import cgtools.Group;
import cgtools.Image;
import cgtools.Shape;
import cgtools.shapes.Background;
import cgtools.shapes.Plane;
import cgtools.shapes.Sphere;

import static cgtools.Vector.*;
import static cgtools.Material.*;

public class Main {

    public static void main(String[] args) {
        
        int width = 3840;
        int height = 2160;

        //int width = 1920;
        //int height = 1020;

        //int width = 1024;
        //int height = 768;

        //int width = 480;
        //int height = 270;
        
//---Scene1-------------------------------------------------------------------------------------------------------------------
       
        Shape background = new Background(BACKGROUND_MATERIAL_BLACK);
        Shape ground = new Plane(point(0, -30, -150), 500.0, direction(0, 1, 0.15), LAMBERT_MATERIAL_GREY);
        Shape ground2 = new Plane(point(-25, 0, -130), 35.0, direction(0, 1, 0.15), LAMBERT_MATERIAL_ORANGE);
        Shape ground3 = new Plane(point(-25, 10, -130), 30.0, direction(0, 1, 0.2), LAMBERT_MATERIAL_ORANGE);
        Shape ground4 = new Plane(point(-25, -10, -130), 25.0, direction(0, 1, 0.15), LAMBERT_MATERIAL_ORANGE);
        Shape ground5 = new Plane(point(-60, -25, -125), 16.5, direction(0, 1, 0.2), GLOWING_MATERIAL_VIOLET);
        Shape ground6 = new Plane(point(30, 22, -125), 11.3, direction(0.15, 1, 1), GLOWING_MATERIAL_GREEN);
        Shape ground7 = new Plane(point(45, -20, -125), 27.0, direction(0, 1, 0.15), GLOWING_MATERIAL_WHITE);
        Shape ground8 = new Plane(point(-15, -33.4, -125), 26.0, direction(0, 1, 0.15), GLOWING_MATERIAL_BLUE);
        Shape globe1 = new Sphere(25, point(-15, -40, -125), MATT_MIRROR_1);
        Shape globe2 = new Sphere(20, point(-25, 0, -125), GLOWING_MATERIAL_WHITE);
        //Shape globe3 = new Sphere(15, point(-60, -25, -125), LAMBERT_MATERIAL_RED);
        Shape globe3 = new Sphere(15, point(-60, -25, -125), PERFECT_MIRROR);
        Shape globe4 = new Sphere(25, point(45, -20, -125), LAMBERT_MATERIAL_BLUE);
        Shape globe5 = new Sphere(10, point(30, 22, -125), LAMBERT_MATERIAL_GREY);
        //Shape globe7 = new Sphere(2, point(50, 33, -130), GLOWING_MATERIAL_VIOLET);
        //Shape globe8 = new Sphere(3, point(53, 15, -110), GLOWING_MATERIAL_GREEN);
        
        Cam c = new Cam(1, point(0, 0, 0), width, height);

        Group group = new Group(new Shape[]{ground, ground2, ground3, ground4, ground5, ground6,
            background, globe1, globe2, globe3, globe4, globe5, ground7, ground8});

        RayTracer r1 = new RayTracer(c, group);
        
//---Scene2-------------------------------------------------------------------------------------------------------------------       
        
        Shape bg = new Background(BACKGROUND_MATERIAL_WHITE);
        Shape gr = new Plane(point(0, 0, -300), 500.0, direction(0, 1, 0.15), LAMBERT_MATERIAL_GREY);
        Shape g1 = new Sphere(20, point(-50, -2, -150), LAMBERT_MATERIAL_ORANGE);
        Shape g2 = new Sphere(25, point(0, 0, -230), PERFECT_MIRROR);
        Shape g3 = new Sphere(20, point(70, 15, -300), LAMBERT_MATERIAL_RED);

        Cam c2 = new Cam(1, point(0, 0, 0), width, height);

        Group group2 = new Group(new Shape[]{bg, gr, g1, g2, g3});

        RayTracer r2 = new RayTracer(c2, group2);

//----------------------------------------------------------------------------------------------------------------------
        
        //image constructor anpassen
        Image image1 = new Image(width, height, new double[width * height * 3], 100);
        image1.sample(r1, true);
        
        final String filename1 = "doc/a05-diffuse-spheres-4k.png";
        image1.write(filename1);
        System.out.println("Wrote image: " + filename1);

    }
}
