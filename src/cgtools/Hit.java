/** @author rabannoack@web.de */
package cgtools;

import static cgtools.Vector.*;
import static cgtools.Util.*;
import static cgtools.Material.*;

public class Hit {
    
    public final Ray t;
    public final Point hitPoint;
    public final Direction normalenVector;
    public final Material material;
    public final double u, v;

    public Hit (Ray t, Point hitPoint, Direction normalenVector, Material material, double u, double v){
        this.t = t;
        this.hitPoint = hitPoint;
        this.normalenVector = normalenVector;
        this.material = material;
        this. u = u;
        this.v = v;
    }

    public Hit(){
        this(null, Vector.point(0, 0, 0), null, null, 0, 0);
    }

    public static Hit defaultHit(Ray r, Point x, Direction n){
        return new Hit(r, x, n, LAMBERT_MATERIAL_BLUE, 0, 0);
    }

    public static Hit infiniteHit(Ray r){
        return new Hit(r, point(I, I, I), normalize(r.direction), BACKGROUND_MATERIAL_WHITE, 0, 0);
    }
}
