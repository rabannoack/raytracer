/** @author rabannoack@web.de */
package cgtools;

import static cgtools.Vector.*;

import java.util.ArrayList;
import java.util.List;

import cgtools.shapes.Background;

//import static cgtools.Hit.*;

public class Group implements Shape{

    public final Shape[] shapes;
    public  Transformation t;

    public Group(Transformation t, Shape... shapes){
        this.t = t;
        this.shapes = shapes;
    }

    public Group(Shape... shapes){
        this(new Transformation(Matrix.identity()), shapes);
    }

    @Override
    public Hit intersect(Ray r){

        //Ray-fromWorld-Tranformation
        Ray rTransformed = this.t.transformRay(r);
        
        Hit returnHit = new Hit();
        double dtemp = rTransformed.tMax;

        //ueber alle Elemente der Group iterieren
        for(Shape s : this.shapes){

            //Pruefen, ob transformierter Strahl transformierte Box trifft
            //BoundingBox box = s.bounds();
            //if(!(box.intersect(r))) continue;

            //Shape.intersect(Ray) des jeweiligen Shapes aufrufen
            Hit h = s.intersect(rTransformed);
            if(h == null) continue;

            //distance 
            double distance = distance(rTransformed.ursprung, h.hitPoint);

            //wenn kleiner als bisheriger Abstand als dtemp festlegen
            //aktuellen hit als return festlegen
            if(distance <= dtemp){
                returnHit = h;
                dtemp = distance;
            }
        }
        //if(returnHit.material == null) return infiniteHit(r);
        if(returnHit.material == null){
            Background b = new Background(Material.TEXTURE_BACKGROUND_04);
            return b.intersect(r);
        } 
        //Hit-toWorld-Transformation
        return this.t.transformHit(returnHit);
    }

    public static double distance(Point p1, Point p2){
        return length(subtract(p1, p2));
    }

    @Override
    public BoundingBox bounds(){
        BoundingBox b = BoundingBox.empty;
        for(Shape s : this.shapes){
            b = b.extend(s.bounds());
        }
        return b.transform(this.t.toWorld);
    }

    //Loest die Hierarchie des Szenegraphen rekursiv auf
    public static List<Shape> getAllShapes(Group shapes){
        List<Shape> shapeList = new ArrayList<Shape>();
        for(Shape s : shapes.shapes){
            if (s instanceof Group){
                shapeList.addAll(getAllShapes((Group)s));
            } else {
                shapeList.add(new Group(shapes.t, s));
            }
        }
        return shapeList;
    }

    
    private static BoundingBox getBoundingBox(Group group){
        BoundingBox bBox = BoundingBox.empty;
        for(Shape s : group.shapes){
            bBox = bBox.extend(s.bounds());
        }
        return bBox;
    }

    public static Group bvh(Group group){

        //neuer Knoten
        Group knot = new Group();

        //BoundingBox für alle Elemente
        BoundingBox bBox = getBoundingBox(group);

        //Boundingbox teilen
        BoundingBox leftbBox = bBox.splitLeft();
        BoundingBox rigthbBox = bBox.splitRight();

        //Kind-Knoten erzeugen
        Group leftGroup = new Group();
        Group rightGroup = new Group();

        //ueber alle Elemente des Knotens interieren und entweder einem Kind-Knoten (left/rigth) zuteilen
        //oder in Knoten belassen
        for(Shape s : group.shapes){

            BoundingBox shapeBounds = s.bounds();

            if (leftbBox.contains(shapeBounds)){
                leftGroup = leftGroup.addShape(s);

            } else if (rigthbBox.contains(shapeBounds)){
                rightGroup = rightGroup.addShape(s);

            } else {
                knot = knot.addShape(s);
            }
        }

        //rekursiver Aufruf der dieser Methode
        if (leftGroup.shapes.length > 1) leftGroup = bvh(leftGroup);
        if (rightGroup.shapes.length > 1) rightGroup = bvh(rightGroup);
        /*if (knot.shapes.length > 1) knot = bvh(knot);*/
        
        knot = knot.addShape(leftGroup);
        knot = knot.addShape(rightGroup);
        return knot;
    }

    //TODO Transformationen multiplizieren
    private Group addShape(Shape s){

        int newLenght = this.shapes.length + 1;

        Group returnGroup = new Group(this.t, new Shape[newLenght]);

        for(int i = 0; i < newLenght; i++){

            if(i < this.shapes.length){
                returnGroup.shapes[i] = this.shapes[i];
                returnGroup.t = multiplyTransformation(returnGroup, (Group)shapes[i]);

            } else {
                returnGroup.shapes[i] = s;
                returnGroup.t = multiplyTransformation(returnGroup, (Group)s);
            }
        }
        return returnGroup;
    }

    private static Transformation multiplyTransformation(Group g1, Group g2){
        return new Transformation(Matrix.multiply(g1.t.toWorld, g1.t.toWorld));
    }
}
