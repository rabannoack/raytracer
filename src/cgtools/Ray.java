/** @author rabannoack@web.de */
package cgtools;

import static cgtools.Vector.*;
import static cgtools.Util.*;

public class Ray {
    
    public Point ursprung;
    public final double tMin, tMax;
    public final Direction direction;

    public Ray (Point ursprung, Direction direction, double tMin, double tMax){
        this.ursprung = ursprung;
        this.direction = direction;
        this.tMin = tMin;
        this.tMax = tMax;
    }

    public Ray(Point ursprung, Direction direction){
        this(ursprung, direction, 0, I);
    }

    public Point pointAt(double t){
        return add(this.ursprung, multiply(t, this.direction));
    }

    public boolean isValid(double t){
        return (this.tMin <= t && t <= this.tMax);
    }

    @Override
    public String toString(){
        double x = this.direction.x;
        double y = this.direction.y;
        double z = this.direction.z;
        return String.format("(X:%.2f Y:%.2f Z:%.2f)", x, y, z);
    }    
}
