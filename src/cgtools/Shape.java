/** @author rabannoack@web.de */
package cgtools;

public interface Shape {
    public Hit intersect(Ray r);
    public BoundingBox bounds();
}
