/** @author rabannoack@web.de */
package cgtools;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Image {
  
  public final int width, height, abtastrate;
  public String filename;
  public final double[] data;

  public Image(int width, int height, double[] data, int abtastrate) {
    this.width = width;
    this.height = height;
    this.data = data;
    this.abtastrate = abtastrate;
  }

  //Default Constructor
  public Image(){
    this(480, 270, new double[480 * 270 * 3], 100);
  }

  public void setPixel(int x, int y, Color color) {
    int index = (y * this.width + x) * 3;
    //gammakorrektur
    color = color.gammaCorrect();
    this.data[index] = color.r;
    this.data[index + 1] = color.g;
    this.data[index + 2] = color.b;
  }

  public void setPixelSmall(int x, int y, int smallWidth,Color color, int divideWidth){
    int index = (y * divideWidth * smallWidth + x) * 3;
    //gammakorrektur
    color = color.gammaCorrect();
    this.data[index] = color.r;
    this.data[index + 1] = color.g;
    this.data[index + 2] = color.b;
  }

  public void write(String filename) {
    ImageWriter.write(filename, this.data, this.width, this.height);
  }

  private Color stratifiedColor(int x, int y, Sampler s) {
    double n = Math.sqrt(this.abtastrate);
    Color c = Color.black;
    for (int xi = 0; xi < n; xi++){
      for (int yi = 0; yi < n; yi++){
        double rx = Random.random();
        double ry = Random.random();
        double xs = x + (xi + rx) / n;
        double ys = y + (yi + ry) / n;
        c = Color.add(c, s.getColor(xs, ys));
      }
    }
    //Mittelwert fuer jeden Farbanteil bilden
    return Color.divide(c, this.abtastrate);
  }

  public void sample(Sampler s, boolean antialiasing) {

    ExecutorService pool = Executors.newFixedThreadPool(8);
    //leere Liste mit Future Objekten
    List<Future<Color>> futures = new ArrayList<>();

    for (int x = 0; x != this.width; x++) {
      for (int y = 0; y != this.height; y++) {
        if(antialiasing) {
            int x2 = x;
            int y2 = y;
            futures.add(pool.submit(() -> stratifiedColor(x2, y2, s)));
            //setPixel(x, y, stratifiedColor(x, y, s));//stratified Sampling
        } else {
            setPixel(x, y, s.getColor(x, y));//point Sampling
        }
      }
    }
    int i = 0;
    for (int x = 0; x != this.width; x++) {
      for (int y = 0; y != this.height; y++) {
        try {
          setPixel(x, y, futures.get(i).get());
        } catch (InterruptedException e) {
          e.printStackTrace();
        } catch (ExecutionException e) {
          e.printStackTrace();
        }
        i++;
      }
    }
    pool.shutdown();
  }
}
