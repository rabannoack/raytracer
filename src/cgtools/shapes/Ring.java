/** @author rabannoack@web.de */
package cgtools.shapes;

import static cgtools.Vector.*;

import cgtools.BoundingBox;
import cgtools.Direction;
import cgtools.Hit;
import cgtools.Material;
import cgtools.Point;
import cgtools.Ray;

public class Ring extends Plane{
 
    public final Double width;

    public Ring(Point anchor, Double radius, Double width, Direction n, Material material){
        super(anchor, radius, n, material);
        this.width = width;
    }

    @Override
    public Hit intersect(Ray r) {
        Hit h = super.intersect(r);
        if(h == null) return null;
        if (length(subtract(this.anchor, h.hitPoint)) < this.radius - this.width) return null;
        return h;
    }

    @Override
    public BoundingBox bounds(){
        return super.bounds();
    }
}
