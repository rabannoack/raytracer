/** @author rabannoack@web.de */
package cgtools.shapes;

import static cgtools.Vector.*;

import cgtools.BoundingBox;

import cgtools.Group;
import cgtools.Hit;
import cgtools.Material;
import cgtools.Point;
import cgtools.Ray;
import cgtools.Shape;

public class Cylinder implements Shape{

    public final Plane top, bottom;
    public final HollowCylinder hc;
    public final Group group;

    public Cylinder(double radius, double height, Point position, Material material){
        this.top = new Plane(add(position, direction(0, height, 0)), radius, direction(0, 1, 0), material);
        this.bottom = new Plane(position, radius, direction(0, 1, 0), material);
        this.hc = new HollowCylinder(radius, height, position, material);
        this.group = new Group(new Shape[]{this.top, this.bottom, this.hc});
    }

    @Override
    public Hit intersect(Ray r) {
        return this.group.intersect(r);
    }

    @Override
    public BoundingBox bounds() {
        return this.hc.bounds();
    }
}