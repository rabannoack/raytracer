/** @author rabannoack@web.de */
package cgtools.shapes;

import static cgtools.Vector.*;

import cgtools.BoundingBox;
import cgtools.Direction;
import cgtools.Hit;
import cgtools.Material;
import cgtools.Point;
import cgtools.Ray;
import cgtools.Shape;

public class Plane implements Shape{

    public final Point anchor;
    public final Double radius;
    public final Direction n;
    public final Material material;

    public Plane(Point anchor, Double radius, Direction n, Material material){
        this.anchor = anchor;
        this.radius = radius;
        this.n = n;
        this.material = material;
    }

    public Double getT(Ray r){
        double p = dotProduct(r.direction, this.n);
        if (p == 0) return null;
        return dotProduct(subtract(this.anchor, r.ursprung), this.n) / p;
    }

    @Override
    public Hit intersect(Ray r){
        double t = getT(r);
        Point hitPoint = r.pointAt(t);
        if (!r.isValid(t)) return null;
        if (length(subtract(this.anchor, hitPoint)) > this.radius) return null;

        double u = hitPoint.x / radius + 0.5;
        double v = hitPoint.z / radius + 0.5;

        if (u > 1) u = u - Math.floor(u);
        if (u < 0) u = u - Math.floor(u);
        
        if (v > 1) v = v - Math.floor(v);
        if (v < 0) v = v - Math.floor(v);

        return new Hit(r, hitPoint, this.n, this.material, u, v);
    }

    @Override
    public BoundingBox bounds(){
        Point min = add(this.anchor, direction(-this.radius, -this.radius, -this.radius));
        Point max = add(this.anchor, direction(this.radius, this.radius, this.radius));
        return new BoundingBox(min, max);
    }  
}
