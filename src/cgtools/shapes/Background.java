/** @author rabannoack@web.de */
package cgtools.shapes;

import cgtools.BoundingBox;
import cgtools.Hit;
import cgtools.Material;
import cgtools.Ray;
import cgtools.Shape;

public class Background implements Shape {

    public final Material material;

    public Background (Material material){
        this.material = material;
    }

    @Override
    public Hit intersect(Ray r) {
        Sphere s = new Sphere(1000, this.material);
        return s.intersect(r);
    }

    @Override
    public BoundingBox bounds() {
        return BoundingBox.everything;
    }
}
