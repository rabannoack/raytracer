/** @author rabannoack@web.de */
package cgtools.shapes;

import static cgtools.Vector.*;

import cgtools.BoundingBox;
import cgtools.Group;
import cgtools.Hit;
import cgtools.Material;
import cgtools.Point;
import cgtools.Ray;
import cgtools.Shape;

public class Pipe implements Shape{

    public final HollowCylinder outer, inner;
    public final Ring top, bottom;
    public final Group group;
 
    public Pipe(double radius,double width, double height, Point position, Material material){
        this.outer = new HollowCylinder(radius, height, position, material);
        this.inner = new HollowCylinder(radius - width, height, position, material);
        this.top = new Ring(add(position, direction(0, height, 0)), radius, width, direction(0, 1, 0), material);
        this.bottom = new Ring(position, radius, width, direction(0, 1, 0), material);
        this.group = new Group(new Shape[]{this.outer, this.inner, this.bottom, this.top});
    }

    @Override
    public Hit intersect(Ray r) {
        return this.group.intersect(r);
    }

    @Override
    public BoundingBox bounds() {
        return this.outer.bounds();
    }
}
