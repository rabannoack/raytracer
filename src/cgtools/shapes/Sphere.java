/** @author rabannoack@web.de */
package cgtools.shapes;

import static cgtools.Vector.*;
import static cgtools.Material.*;
//import static cgtools.Matrix.*;
import java.util.Arrays;

import cgtools.BoundingBox;
import cgtools.Direction;
import cgtools.Group;
import cgtools.Hit;
import cgtools.Material;
import cgtools.Point;
import cgtools.Random;
import cgtools.Ray;
import cgtools.Shape;
//import cgtools.Transformation;

public class Sphere implements Comparable<Sphere>, Shape{
    public final double radius;
    public final Point center;
    public final Material material;

    public Sphere(double radius, Point center, Material material){
        this.radius = radius;
        this.center = center;
        this.material = material;
    }

    public Sphere(double radius, Material material){
        this(radius, zero, material);
    }

    private Point moveXNull(Ray r){
        Direction d = subtract(zero, this.center);
        return add(r.ursprung, d);
    }

    private Direction getNormalenvektor(Point p){
        return normalize(divide(subtract(p, this.center), this.radius));
    }

    private double[] getT(Direction dir, Point p){
        double a = dotProduct(dir, dir);
        double b = 2 * dotProduct(p, dir);
        double c = dotProduct(p, p) - Math.pow(this.radius, 2);
        double d = Math.pow(b, 2) - 4 * a * c;
        if (d < 0){
            return new double[]{-1, -1};
        } else if(d == 0){
            return new double[]{(-b - Math.sqrt(d)) / (2 * a), -1};
        } else {
            double tMinus = (-b - Math.sqrt(d)) / (2 * a);
            double tPlus = (-b + Math.sqrt(d)) / (2 * a);
            return new double[]{tMinus, tPlus};
        }
    }

    public static Sphere[] getRandomSpheres(int n, int maxRadius, int width, int height, int z, boolean sort, Material material){
        Sphere[] spheres = new Sphere[n];
        for (int i = 0; i < spheres.length; i++){
            spheres[i] = getRandomSphere(maxRadius, width, height, z, material);
        }
        if(sort) Arrays.sort(spheres);
        return spheres;
    }

    public static Sphere getRandomSphere(int maxRadius, int width, int height, int z, Material material){

        int x = new Random().nextInt(width / 7);
        boolean xMinus = new Random().nextBoolean();
        if(xMinus) x *= -1;
            
        int y = new Random().nextInt(height / 7);
        boolean yMinus = new Random().nextBoolean();
        if(yMinus) y *= -1;
            
        Point center = point(x, y, z);
        int radius = new Random().nextInt(maxRadius);
        return new Sphere(radius, center, material);
    }

    public static Shape makeSpheres(Point position, int exponent) {
        if (exponent <= 0) {
            //return new Sphere(0.9, position, randomMaterial());
            return new Sphere(0.9, position, nextMaterial());
        } else {
            double offset = Math.pow(2, exponent) / 2;
            //double rotate = new Random().nextInt(90);
            return new Group(
                makeSpheres(add(position, direction(-offset, 0.0, -offset)),
                    exponent - 1),
                makeSpheres(add(position, direction(-offset, 0.0, +offset)),
                    exponent - 1),
                makeSpheres(add(position, direction(+offset, 0.0, -offset)),
                    exponent - 1),
                makeSpheres(add(position, direction(+offset, 0.0, +offset)),
                    exponent - 1)); 
        }
    }

    @Override
    public Hit intersect(Ray r){
        //Strahl verschieben und t berechnen
        Point xNull = moveXNull(r);
        Direction d = r.direction;
        double[] values = getT(d, xNull);
        double t0 = values[0];
        double t1 = values[1];
        
        if(r.isValid(t0)){
            Point x = r.pointAt(t0);
            Direction n = getNormalenvektor(x);

            double azimuth = Math.PI + Math.atan2(n.x, n.z);
            double u = azimuth / (2 * Math.PI);

            double inclination = Math.acos(n.y);
            double v = inclination / Math.PI;

            if (u > 1) u = u - Math.floor(u);
            if (u < 0) u = u - Math.floor(u);
        
            if (v > 1) v = v - Math.floor(v);
            if (v < 0) v = v - Math.floor(v); 

            return new Hit(r, x, n, this.material, u, v);

        } else if (r.isValid(t1)){
            Point x = r.pointAt(t1);
            Direction n = getNormalenvektor(x);

            double azimuth = Math.PI + Math.atan2(n.x, n.z);
            double u = azimuth / (2 * Math.PI);

            double inclination = Math.acos(n.y);
            double v = inclination / Math.PI;

            if (u > 1) u = u - Math.floor(u);
            if (u < 0) u = u - Math.floor(u);
        
            if (v > 1) v = v - Math.floor(v);
            if (v < 0) v = v - Math.floor(v);
            
            return new Hit(r, x, n, this.material, u, v);
            
        } else {
            return null;
        }
    }

    @Override
    public int compareTo(Sphere s) {
        return Double.compare(this.radius, s.radius);
    }

    @Override
    public BoundingBox bounds() {
        Point min = add(this.center, direction(-this.radius, -this.radius, -this.radius));
        Point max = add(this.center, direction(this.radius, this.radius, this.radius));
        return new BoundingBox(min, max);
    }  
}
