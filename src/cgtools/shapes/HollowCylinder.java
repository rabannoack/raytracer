/** @author rabannoack@web.de */
package cgtools.shapes;

import static cgtools.Vector.*;

import cgtools.BoundingBox;
import cgtools.Direction;
import cgtools.Hit;
import cgtools.Material;
import cgtools.Point;
import cgtools.Ray;
import cgtools.Shape;

public class HollowCylinder implements Shape{

    public final double radius, height;
    public final Point position;
    public final Material material;

    public HollowCylinder(double radius, double height, Point position, Material material){
        this.radius = radius;
        this.height = height;
        this.position = position;
        this.material = material;
    }

    //Quellen:  - https://www.math.uni-trier.de//~schulz/prosem0708/Raytracing.pdf [02.06.2021]
    //          - https://www.c-plusplus.net/forum/topic/178915/schnittpunkte-f%C3%BCr-zylinder-im-raytracer/2 [02.06.2021]
    //          - https://github.com/Daniel6/Ray-Tracing/blob/master/Ray%20Tracing/src/Cylinder.java [02.06.2021]
    @Override
    public Hit intersect(Ray r) {
        
        Point xNull = moveXNull(r);
        Direction d = r.direction;
        double[] values = getT(r, d, xNull);
        double t0 = values[0];
        double t1 = values[1];

        if(r.isValid(t0)){
            Point x = r.pointAt(t0);
            Direction n = getNormalenvektor(x);

            //double azimuth = Math.PI + Math.atan2(n.x, n.y);
            double azimuth = Math.PI + Math.atan2(n.x, n.z);
            double u = azimuth / (2 * Math.PI);

            double v = x.y / (2 * Math.PI);
            v = v - Math.floor(v);
            //double v = inclination / Math.PI;

            if (u > 1) u = u - Math.floor(u);
            if (u < 0) u = u - Math.floor(u);
        
            if (v > 1) v = v - Math.floor(v);
            if (v < 0) v = v - Math.floor(v); 

            return new Hit(r, x, n, material, u, v);

        } else if (r.isValid(t1)){
            Point x = r.pointAt(t1);
            Direction n = getNormalenvektor(x);

            //double azimuth = Math.PI + Math.atan2(n.x, n.y);
            double azimuth = Math.PI + Math.atan2(n.x, n.z);
            double u = azimuth / (2 * Math.PI);

            double v = x.y / (2 * Math.PI);
            v = v - Math.floor(v);
            //double v = inclination / Math.PI;

            if (u > 1) u = u - Math.floor(u);
            if (u < 0) u = u - Math.floor(u);
        
            if (v > 1) v = v - Math.floor(v);
            if (v < 0) v = v - Math.floor(v); 

            return new Hit(r, x, n, material, u, v);

        } 
        return null;
    }

    private double[] getT(Ray r, Direction d, Point p){

        double a = dotProductXZ(d, d);
        double b = 2 * dotProductXZ(p, d);
        double c = dotProductXZ(p, p) - Math.pow(this.radius, 2);
        double discriminant = Math.pow(b, 2) - 4 * a * c;
        
        if (discriminant < 0){
            return new double[]{-1, -1};
        } else if(discriminant == 0){
            double t0 = (-b - Math.sqrt(discriminant)) / (2 * a);
            if(isWithinHeight(r, t0)){
                return new double[]{t0 , -1};
            } else {
                return new double[]{-1, -1};
            }
        } else {
            double t0 = (-b - Math.sqrt(discriminant)) / (2 * a);
            double t1 = (-b + Math.sqrt(discriminant)) / (2 * a);
            double[] values = new double[2];
            if(isWithinHeight(r, t0)){
                values[0] = t0;
            } else {
                values[0] = -1;
            }
            if(isWithinHeight(r, t1)){
                values[1] = t1;
            } else {
                values[1] = -1;
            }
            return values;
        }
    }

    private boolean isWithinHeight(Ray r, double t){
        Point p = r.pointAt(t);
        return(this.position.y <= p.y && p.y <= this.position.y + this.height);
    }

    private Point moveXNull(Ray r){
        Direction d = subtract(zero, this.position);
        return add(r.ursprung, d);
    }

    private Direction getNormalenvektor(Point p){
        Point py = add(this.position, direction(0, (p.y - this.position.y), 0));
        return normalize(divide(subtract(p, py), this.radius));
    }

    private double dotProductXZ(Direction a, Direction b) {
        return a.x * b.x + a.z * b.z;
      }

    private double dotProductXZ(Point a, Direction b) {
        return a.x * b.x + a.z * b.z;
    }

    private double dotProductXZ(Point a, Point b) {
        return a.x * b.x + a.z * b.z;
    }

    @Override
    public BoundingBox bounds() {
        Point min = add(this.position, direction(-this.radius, -this.radius, -this.radius));
        Point max = add(this.position, direction(this.radius, this.height + this.radius, this.radius));
        return new BoundingBox(min, max);
    }    
}
