/** @author rabannoack@web.de */
package cgtools;

import static cgtools.Color.*;

public class RayTracer implements Sampler{ 
    
    public final Cam cam;
    public final Group group;

    public RayTracer(Cam c, Group group){
        this.cam = c;
        this.group = group;
    }

    @Override
    public Color getColor(double x, double y) {
        Ray r = this.cam.getRay(x, y);
        return radiance(r, this.group, 5);
    }

    private static Color radiance(Ray r, Group group, int depth){

        if(depth == 0) return Color.black;

        Hit h = group.intersect(r);
        //Color c = Color.black;

        if(!h.material.isEmmitting()){
            //c = add(radiance(h.material.reflectRay(r, h), group, depth - 1), c);
            //return multiply(h.material.albedo().getColor(h.u, h.v), c);
            Color c = radiance(h.material.reflectRay(r, h), group, depth - 1);
            return multiply(add(h.material.emission().getColor(h.u, h.v), h.material.albedo().getColor(h.u, h.v)), c);
            
        } else {
            return h.material.emission().getColor(h.u, h.v);
        }
    }
}
