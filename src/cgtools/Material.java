/** @author rabannoack@web.de */
package cgtools;

import static cgtools.Reflection.*;
import static cgtools.Vector.*;

public interface Material {

    public static final Material LAMBERT_MATERIAL_GREY = new Material(){

        public boolean isEmmitting(){
            return false;
        }
        public Ray reflectRay(Ray r, Hit h){
            return lambertReflection(r, h);
        }
        public Sampler albedo(){
            return new Constant(color(0.8, 0.8, 0.8));
        }
        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public static final Material LAMBERT_MATERIAL_BLUE = new Material(){

        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return lambertReflection(r, h);
        }

        public Sampler albedo(){
            return new Constant(Color.blue);
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public static final Material LAMBERT_MATERIAL_RED = new Material(){

        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return lambertReflection(r, h);
        }

        public Sampler albedo(){
            return new Constant(Color.red);
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public static final Material LAMBERT_MATERIAL_ORANGE = new Material(){

        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return lambertReflection(r, h);
        }

        public Sampler albedo(){
            return new Constant(color(1, 0.65, 0));
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public static final Material LAMBERT_MATERIAL_ROSA = new Material(){

        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return lambertReflection(r, h);
        }

        public Sampler albedo(){
            return new Constant(color(0.9411, 0.5019, 0.5019));
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public static final Material GLOWING_MATERIAL_WHITE = new Material(){

        public boolean isEmmitting(){
            return true;
        }

        public Ray reflectRay(Ray r, Hit h){
            return null;
        }

        public Sampler albedo(){
            return new Constant(Color.black);
        }

        public Sampler emission(){
            return new Constant(Color.white);
        }
    };

    public static final Material GLOWING_MATERIAL_GREEN = new Material(){

        public boolean isEmmitting(){
            return true;
        }

        public Ray reflectRay(Ray r, Hit h){
            return null;
        }

        public Sampler albedo(){
            return new Constant(Color.black);
        }

        public Sampler emission(){
            return new Constant(Color.green);
        }
    };

    public static final Material GLOWING_MATERIAL_VIOLET= new Material(){

        public boolean isEmmitting(){
            return true;
        }

        public Ray reflectRay(Ray r, Hit h){
            return null;
        }

        public Sampler albedo(){
            return new Constant(Color.black);
        }

        public Sampler emission(){
            return new Constant(color(1, 0, 1));
        }
    };

    public static final Material GLOWING_MATERIAL_BLUE = new Material(){

        public boolean isEmmitting(){
            return true;
        }

        public Ray reflectRay(Ray r, Hit h){
            return null;
        }

        public Sampler albedo(){
            return new Constant(Color.black);
        }

        public Sampler emission(){
            return new Constant(color(0.5, 1, 0.83));
        }
    };

    public static final Material GLOWING_MATERIAL_RED = new Material(){

        public boolean isEmmitting(){
            return true;
        }

        public Ray reflectRay(Ray r, Hit h){
            return null;
        }

        public Sampler albedo(){
            return new Constant(Color.black);
        }

        public Sampler emission(){
            return new Constant(Color.red);
        }
    };

    public static final Material BACKGROUND_MATERIAL_BLACK = new Material(){
        
        public boolean isEmmitting(){
            return true;
        }

        public Ray reflectRay(Ray r, Hit h){
            return null;
        }

        public Sampler albedo(){
            return new Constant(color(0.1, 0.1, 0.1));
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public static final Material BACKGROUND_MATERIAL_GREY = new Material(){
        
        public boolean isEmmitting(){
            return true;
        }

        public Ray reflectRay(Ray r, Hit h){
            return null;
        }

        public Sampler albedo(){
            return new Constant(Color.black);
        }

        public Sampler emission(){
            return new Constant(color(0.5, 0.5, 0.5));
        }
    };

    public static final Material BACKGROUND_MATERIAL_WHITE = new Material(){
        
        public boolean isEmmitting(){
            return true;
        }

        public Ray reflectRay(Ray r, Hit h){
            return null;
        }

        public Sampler albedo(){
            return new Constant(Color.black);
        }

        public Sampler emission(){
            return new Constant(Color.white);
        }
    };

    public static final Material PERFECT_MIRROR = new Material(){
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return mirrorReflection(r, h, true, 0);
        }

        public Sampler albedo(){
            return new Constant(Color.white);
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public static final Material MATT_MIRROR_1 = new Material(){
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return mirrorReflection(r, h, false, 0.3);
        }

        public Sampler albedo(){
            return new Constant(Color.white);
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public static final Material MATT_MIRROR_2 = new Material(){
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return mirrorReflection(r, h, false, 0.9);
        }

        public Sampler albedo(){
            return new Constant(Color.white);
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public static final Material MATT_MIRROR_BLUE = new Material(){
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return mirrorReflection(r, h, false, 0.4);
        }

        public Sampler albedo(){
            return new Constant(color(0.5, 1, 0.83));
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public static final Material GLASS = new Material(){
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return fresnelReflection(r, h, 1, 1.5, true);
        }

        public Sampler albedo(){
            return new Constant(Color.white);
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public static final Material GLASS_BROWN = new Material(){
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return fresnelReflection(r, h, 1, 1.5, true);
        }

        public Sampler albedo(){
            return  new Constant(color(0.696, 0.563, 0.429));
        }

        public Sampler emission(){
            return new Constant((Color.black));
        }
    };

    public static final Material GLASS_LIGHT_PURPLE = new Material(){
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return fresnelReflection(r, h, 1, 1.5, true);
        }

        public Sampler albedo(){
            return new Constant(color(0.863, 0.796, 0.933));
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public static final Material GLASS_LIGHT_GREEN = new Material(){
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return fresnelReflection(r, h, 1, 1.5, true);
        }

        public Sampler albedo(){
            return new Constant(color(0.725, 0.91, 0.722));
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public final Material GLASS_LIGHT_BLUE = new Material(){
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return fresnelReflection(r, h, 1, 1.5, true);
        }

        public Sampler albedo(){
            return new Constant(color(0.5, 1, 0.83));
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public final Material TEXTURE_01 = new Material(){

        private final ImageTexture iTexture = new ImageTexture("doc/a01-polka-dots.png");
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return lambertReflection(r, h);
        }

        public Sampler albedo(){
            return iTexture;
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public final Material TEXTURE_02 = new Material(){

        private final ImageTexture iTexture = new ImageTexture("doc/swiss-grass.jpg");
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return lambertReflection(r, h);
        }

        public Sampler albedo(){
            return iTexture;
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public final Material TEXTURE_03 = new Material(){

        private final ImageTexture iTexture = new ImageTexture("doc/a02-discs-supersampling.png");
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return lambertReflection(r, h);
        }

        public Sampler albedo(){
            return iTexture;
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public final Material TEXTURE_04 = new Material(){

        private final ImageTexture iTexture = new ImageTexture("doc/a03-three-spheres.png");
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return lambertReflection(r, h);
        }

        public Sampler albedo(){
            return iTexture;
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public final Material TEXTURE_05 = new Material(){

        private final ImageTexture iTexture = new ImageTexture("doc/wall-texture.jpg");
        
        public boolean isEmmitting(){
            return false;
        }

        public Ray reflectRay(Ray r, Hit h){
            return lambertReflection(r, h);
        }

        public Sampler albedo(){
            return iTexture;
        }

        public Sampler emission(){
            return new Constant(Color.black);
        }
    };

    public final Material TEXTURE_BACKGROUND_01 = new Material(){

        private final ImageTexture iTexture = new ImageTexture("doc/sky-texture.jpg");
        
        public boolean isEmmitting(){
            return true;
        }

        public Ray reflectRay(Ray r, Hit h){
            return null;
        }

        public Sampler albedo(){
            return null;
        }

        public Sampler emission(){
            return iTexture;
        }
    };

    public final Material TEXTURE_BACKGROUND_02 = new Material(){

        private final ImageTexture iTexture = new ImageTexture("doc/sky-texture-2.jpg");
        
        public boolean isEmmitting(){
            return true;
        }

        public Ray reflectRay(Ray r, Hit h){
            return null;
        }

        public Sampler albedo(){
            return null;
        }

        public Sampler emission(){
            return iTexture;
        }
    };

    public final Material TEXTURE_BACKGROUND_03 = new Material(){

        private final ImageTexture iTexture = new ImageTexture("doc/sky-texture-2.jpg");
        
        public boolean isEmmitting(){
            return true;
        }

        public Ray reflectRay(Ray r, Hit h){
            return null;
        }

        public Sampler albedo(){
            return null;
        }

        public Sampler emission(){
            return iTexture;
        }
    };

    public final Material TEXTURE_BACKGROUND_04 = new Material(){

        private final ImageTexture iTexture = new ImageTexture("doc/thunder.jpg");
        
        public boolean isEmmitting(){
            return true;
        }

        public Ray reflectRay(Ray r, Hit h){
            return null;
        }

        public Sampler albedo(){
            return null;
        }

        public Sampler emission(){
            return iTexture;
        }
    };

    /*
    public class Mirror implements Material(Ray r, Hit h){
    }
    */

    public static final Material[] MATERIALS = {
            GLASS_LIGHT_BLUE,
            GLASS_LIGHT_GREEN,
            GLASS_LIGHT_PURPLE,
            GLASS_BROWN,
            GLASS,
            MATT_MIRROR_BLUE,
            MATT_MIRROR_2,
            MATT_MIRROR_1,
            PERFECT_MIRROR,
            LAMBERT_MATERIAL_ROSA,
            //LAMBERT_MATERIAL_BLUE,
            LAMBERT_MATERIAL_GREY,
            LAMBERT_MATERIAL_ORANGE,
            //LAMBERT_MATERIAL_RED,
            TEXTURE_01,
            //TEXTURE_02,
            TEXTURE_03,
            TEXTURE_04,
            TEXTURE_05,
            TEXTURE_BACKGROUND_01,
            TEXTURE_BACKGROUND_02,
    };

    public static Material randomMaterial(){
        return MATERIALS[new Random().nextInt(MATERIALS.length)];
    }

    public class Counter{
        public static int counter = 0;
    }

    public static Material nextMaterial(){
        if (Counter.counter == MATERIALS.length) Counter.counter = 0;
        Material retMat = MATERIALS[Counter.counter];
        Counter.counter++;
        return retMat;
    }

    class Constant implements Sampler{
        final Color color;

        public Constant(Color color){
            this.color = color;
        }

        @Override
        public Color getColor(double u, double v) {
            return this.color;
        } 
    }

    class Texture implements Sampler{
        final ImageTexture texture;

        public Texture(String filename){
            this.texture = new ImageTexture(filename);
        }

        @Override
        public Color getColor(double u, double v) {
            return this.texture.getColor(u, v);
        } 
    }

    public boolean isEmmitting();

    public Ray reflectRay(Ray r, Hit h);

    public Sampler albedo();

    public Sampler emission();
    
}
