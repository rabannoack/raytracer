package cgtools;

import static cgtools.Matrix.*;

public class Transformation {
    
    public final Matrix toWorld, fromWorld, toWorldN;

    public Transformation(Matrix m){
        this.toWorld = m;
        this.fromWorld = invert(m);
        this.toWorldN = transpose(invert(m));
    }

    public Ray transformRay(Ray r){
        return new Ray(multiply(this.fromWorld, r.ursprung), multiply(this.fromWorld, r.direction), r.tMin, r.tMax);
    }

    public Hit transformHit(Hit h){
        return new Hit(h.t, multiply(this.toWorld, h.hitPoint), multiply(this.toWorldN, h.normalenVector), h.material, h.u, h.v);
    }
}
