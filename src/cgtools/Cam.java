/** @author rabannoack@web.de */
package cgtools;

import static cgtools.Vector.*;
import static cgtools.Matrix.*;

public class Cam {

    public final double angle;
    public final Point center;
    public final int width, height;
    public final Matrix matrix;

    public Cam (double angle,Point center, int width, int height, Matrix matrix){
        this.angle = angle;
        this.width = width;
        this.height = height;
        this.center = center;
        this.matrix = matrix;
    }

    public Cam (double angle,Point center, int width, int height){
        this(angle, center, width, height, null);
    }

    public Ray getRay(double x, double y){
        double newX = x - this.width / 2;
        double newY = -(y - this.height / 2);
        double newZ = -((this.width / 2) / (Math.tan(this.angle / 2)));
        Ray r = new Ray(this.center, normalize(new Direction(newX, newY, newZ)));
        if(this.matrix != null) return transform(r);
        return r;
    }

    /**
     * @param  r takes temporary Ray
     * @return  returns moved camera if Cam-Matrix != null
    */
    private Ray transform(Ray r) {
        return new Ray(multiply(this.matrix, r.ursprung), multiply(this.matrix, r.direction));
    }
}
