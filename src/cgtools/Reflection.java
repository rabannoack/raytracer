/** @author rabannoack@web.de */
package cgtools;

import static cgtools.Vector.*;
import static cgtools.Util.*;

public class Reflection {

    public static final Double E = 1 * Math.pow(10, -4);


//--Mirror_Reflection--
    /**
     * 
     * @param r
     * @param h
     * @param perfect
     * @param radiance  degree of scattering - from 0.0 (perfect mirror) to 1.0 (nearly total diffusion)
     * @return  returns Ray - perfect reflection if (perfect == true) or 
     * scattered reflection depending on "radiance" value
     */
    public static Ray mirrorReflection(Ray r, Hit h, boolean perfect, double radiance){
        Direction n = h.normalenVector;
        Direction d = r.direction;

        Direction b = multiply(dotProduct(negate(d), n), n);
        Point p = add(h.hitPoint, d, b, b);

        Direction dReturn = subtract(p, h.hitPoint);
        if(!perfect){
            dReturn = add(dReturn, multiply(Direction.randomDirection(),radiance));
            if(dotProduct(dReturn, n) < 0) return r; //Direction(0, 0, 0)?
        }
        return new Ray(h.hitPoint, dReturn, E, I);
    }


//--Lambert_Reflection--

    public static Ray lambertReflection(Ray r, Hit h) {
        Direction random = normalize(rejectionSampling());
        Direction d = normalize(add(h.normalenVector, random));
        return new Ray(h.hitPoint, d, E, I);
    }

    private static Direction rejectionSampling(){
        double x = Util.randomCoordinate();
        double y = Util.randomCoordinate();
        double z = Util.randomCoordinate();
        if(x + y + z > 1){
            return rejectionSampling();
        } else {
            return direction(x, y, z);
        }
    }


//--Fresnel_Reflection--

    public static Ray fresnelReflection(Ray r, Hit h, double n1, double n2, boolean refract){
        Direction n = h.normalenVector;
        Direction d = r.direction;
        Direction scattered = new Direction(0, 0, 0);
        if(dotProduct(n, d) > 0){
            double temp = n1;
            n1 = n2;
            n2 = temp;
            n = negate(n);
        }
        if(refract){
            if(new Random().nextDouble() > schlick(d, n, n1, n2)){
                scattered = refract(d, n, n1, n2, h, r);
            } else {
                scattered = mirrorReflection(r, h, true, 0).direction;
            }
        } else {
            scattered = mirrorReflection(r, h, true, 0).direction;
        }
        return new Ray(h.hitPoint, scattered, E, I);
    }
    
    public static Direction refract(Direction d, Direction n, double n1, double n2, Hit h, Ray ray){
        double r = n1 / n2;
        double c = -dotProduct(n, d);
        Direction rd = multiply(d, r);
        double diskr = Math.sqrt(1 - Math.pow(r, 2) * (1 - Math.pow(c, 2)));
        if(diskr < 0) return mirrorReflection(ray, h, true, 0).direction;
        return add(rd, multiply(r * c - diskr, n));
    }

    public static double schlick(Direction d, Direction n, double n1, double n2){
        double rNull = Math.pow(((n1 - n2) / (n1 + n2)), 2);
        return rNull + (1 - rNull) * Math.pow((1 + dotProduct(n, d)), 5);
    }
}
