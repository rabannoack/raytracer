# Raytracer

## Raytracing-Algorithmus für das Modul: Computergrafik Grundlagen

### Hier findest du einige Beispielbilder die ich mit dem Algorithmus gerendert habe

#### Beispiele verschiedener Materialien, Formen und Beleuchtung

![a06-mirrors-glass-1.png](/doc/a06-mirrors-glass-1.png)

![a06-mirrors-glass-2.png](/doc/a06-mirrors-glass-2.png)

![a07-1.png](doc/a07-1.png)

![a07-1-testPipe.png](/doc/a07-1-testPipe.png)

#### Bewegliche Kamera

![a08-1.png](/doc/a08-1.png)

![a08-1.png](/doc/a08-2.png)

#### Multithreading (Laufzeitreduzierung) und Texturen

![a08-2-thread-test_05.png](/doc/a08-2-thread-test_05.png)
